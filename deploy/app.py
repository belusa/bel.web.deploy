import sys
from deploy import GitChangesetDeploy, DeploymentException
from traceback import print_exc
from config import Config
from logger import Logger
from helper import CommandHelper, SFTPHelper, SFTPHelperConnectException, InvalidEnvironmentSuppliedException
#import time

from traceback import print_exc, format_exc

config = Config()
logger = Logger(config)

try:
    # Set current enrivonment
    env = sys.argv[1]

    if env in config.remote_servers:
        config.current_environment = config.remote_servers[env]
    else:
        raise InvalidEnvironmentSuppliedException

    # Init SFTP helper
    sftp_helper = SFTPHelper(config, logger)

    try:
        # Attempt to create a connection to remote server
        sftp_helper.connect()
    except Exception as e:
        logger.log("sftp: An error occurred while connecting to the remote server.")
        logger.log(e)

        raise SFTPHelperConnectException

    # Init Command helper
    command_helper = CommandHelper()

    # Init deployment class
    app = GitChangesetDeploy(config, logger, command_helper, sftp_helper)

    # Deploy package
    try:
        app.deploy()
    except DeploymentException:
        app.rollback()

    # Disconnect from SFTP server
    sftp_helper.disconnect()

    # Send deployment email summary
    #logger.send_log_via_email()
except Exception as e:
    logger.log("An exception occurred during the execution of {}. Details can be found below.".format(config.app_name))

    logger.log({'e': e, 'exc': format_exc()})
finally:
    print("---------------------- Script Completed ----------------------")