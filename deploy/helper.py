import pysftp
import subprocess
from time import sleep
import time
import os

from deploy import DeploymentException

"""
Simple wrapper around pysftp module.

Allows developer to 

@author Raymond Nunez <rnunez@belusallc.com>
"""


class SFTPHelper:

    def __init__(self, config, logger):
        self.config = config
        self.logger = logger

    def connect(self):
        # Init pystp CnOpts object
        # Read more: https://pysftp.readthedocs.io/en/release_0.2.9/cookbook.html#pysftp-cnopts

        #cnopts = pysftp.CnOpts(knownhosts='~/.ssh/known_hosts')
        cnopts = pysftp.CnOpts(knownhosts=os.path.expanduser('~/.ssh/known_hosts'))

        # Connection
        # Read more: https://pysftp.readthedocs.io/en/release_0.2.9/cookbook.html#pysftp-connection

        # Init connection
        if self.config.current_environment["remote_server_use_pem_key"]:
            #cnopts.hostkeys = False

            # Connect using PEM key file
            self.sftp = pysftp.Connection(self.config.current_environment["remote_server_host"], username=self.config.current_environment["remote_server_user"], private_key=self.config.current_environment["remote_server_pem_key_path"], cnopts=cnopts)
        else:
            # We need this set to None (disable host key verification) specifically
            # for dev-ftp.discountmugs and for qa-ftp.discountmugs.com since
            # we are connecting via IP address and the app is being ran
            # from a Bitbucket Pipeline (or anything outside of the BEL network)
            cnopts.hostkeys = False

            # Connect using config credentials
            self.sftp = pysftp.Connection(self.config.current_environment["remote_server_host"], username=self.config.current_environment["remote_server_user"], password=self.config.current_environment["remote_server_pass"], cnopts=cnopts) 

    def disconnect(self):
        try:
            self.sftp.close()
        except Exception as e:
            raise SFTPHelperDisconnectException(e)

    def upload_files(self, files):
        files_len = len(files)
        num_success = 0
        num_deleted = 0
        num_errors = 0
        errors = []

        remote_dir = self.config.current_environment["remote_dir"]
        local_dir = self.config.local_dir

        for file_object in files:
            file_name = file_object['file_path'].replace(remote_dir, "").replace("\\", "/")
            local_file_path = local_dir + "/" + file_name
            destination = remote_dir + "/" + file_name

            remote_file_path = "/".join(destination.split("/")[:-1])

            # TODO store original file contents in case of roll-back

            original_file_contents = ""

            self.logger.log("Processing file: " + file_name +" - Action: "+ file_object['action'])
            self.logger.log("- Local file path: " + local_file_path)
            self.logger.log("- Remote file path: " + destination)

            # If necessary, this command will create all directories, one by one, until
            # all non-existing directories exist on the remote server.
            try:
                self.sftp.makedirs(remote_file_path, self.config.mkdir_permission)
            except Exception as e:
                num_errors += 1

                self.logger.log(e)

            try:
                if file_object['action'] == 'delete':
                    # Attempt to delete file
                    self.sftp.remove(destination)

                    num_deleted += 1

                    self.logger.log("- Deleted file without error")
                elif file_object['action'] == 'upload' or file_object['action'] == 'new':
                    # Attempt to upload file

                    result = self.sftp.put(local_file_path, destination)

                    num_success += 1

                    self.logger.log("- Uploaded file without error:")
                    self.logger.log(result)
            except Exception as e:
                # IOError: destination does not exist
                # OSError: local file does not exist

                num_errors += 1

                #raise DeploymentException

                error_object = {
                    "file": file_object,
                    "local_file_path": local_file_path,
                    "remote_file_Path": remote_file_path,
                    "remote_dir": remote_dir,
                    "message": "Error while processing file. Either the file does not exist locally or on the server.",
                    "exception": e
                }

                errors.append(error_object)

                self.logger.log(error_object)

        return {
            'num_files': files_len,
            'num_success': num_success,
            'num_deleted': num_deleted,
            'num_errors': num_errors,
            'errors': errors
        }


class SFTPHelperConnectException(Exception):
    pass


class SFTPHelperDisconnectException(Exception):
    pass

"""
Command helper
Makes it easy to run a command and manipulate it's output programmatically

@author Raymond Nunez <rnunez@belusallc.com>
"""


class CommandHelper:

    """
    Runs command ```command``` and returns the output of the command line-by-line.

    This does not return the entire output in one function call (unless there is only one line of output).
    This method is meant to be used in a for loop (or anything that can consume one result one at a time)
    """
    def run_command(self, command):
        p = subprocess.Popen(command,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE,
                            shell=True)

        # Read stdout from subprocess until the buffer is empty !
        for line in iter(p.stdout.readline, b''):
            if line:  # Don't print blank lines
                line = line.decode('utf-8')
                line = line.rstrip()  # Strip newline

                yield line

        # This ensures the process has completed, AND sets the 'returncode' attr
        while p.poll() is None:
            sleep(.1)  # Don't waste CPU-cycles

        # Empty STDERR buffer
        err = p.stderr.read()

        if p.returncode != 0:
            # The run_command() function is responsible for logging STDERR
            print("Error: " + str(err))

            raise(RunCommandException(err))


class RunCommandException(Exception):
    pass

class InvalidEnvironmentSuppliedException(Exception):
    pass