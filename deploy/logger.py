"""
Simple logger class.

Adds data to a _log list with log() that can be seen with dump()

@author Raymond Nunez <rnunez@belusallc.com>
"""

# Import smtplib for the actual sending function
import smtplib
import ssl

# Import the email modules we'll need
from datetime import date

class Logger:

    def __init__(self, config):
        self._log = []
        self.config = config

    def log(self, message):
        if self.config.logger["core"]["print_while_logging"]:
            print(message)

        self._log.append(str(message))

    def dump(self):
        for x in self._log:
            print(x)

    def dump_for_email(self):
        # content = ""
        #
        # for x in self._log:
        #     content = content + " " + x + "<br />\n"
        #
        # return content

        for x in self._log:
            print(x)

        return "<br />\n".join(self._log)

    def send_log_via_email(self):
        try:
            server = smtplib.SMTP("email-smtp.us-east-1.amazonaws.com")

            if not server.login(self.config.logger["email"]["smtp_server"], port=587):
                raise EmailConnectException

            app_result = True

            subject_result = "SUCCESS" if app_result == True else "FAILURE"

            email_date = date.today().strftime("%B %d, %Y")

            # me == the sender's email address
            # you == the recipient's email address
            subject = "Deployment Email Result (" + subject_result + ") on " + email_date

            body = """\
            Subject: """ + subject + """
            
            -------------------------------------------------------
            Deployment Result: """ + subject_result + """
            Date/Time: """ + email_date + """
            -------------------------------------------------------
            
            -------------------------------------------------------
            Summary:
            -------------------------------------------------------
            
            """ + self.dump_for_email()

            server.sendmail(self.config.logger["email"]["smtp_user"], self.config.logger["email"]["recipients"], body)

            server.quit()
        except Exception as e:
            self.log("smtp: An error occurred while sending the summary email.")
            self.log(e)

            raise EmailSendException(e)

class EmailConnectException(Exception):
    pass

class EmailSendException(Exception):
    pass