"""
Changeset-based deployment strategy for a Git repository.
The Git repo mentioned throughout is the repo in which this
project resides in.

@author Raymond Nunez <rnunez@belusallc.com>
"""


class GitChangesetDeploy:

    def __init__(self, config, logger, command_helper, sftp_helper):
        self.changeset = []

        self.config = config
        self.logger = logger
        self.command_helper = command_helper
        self.sftp_helper = sftp_helper

    """
    Runs command ```diff-tree``` against the repo and receives a list
    of all files in the most recent commit.
    """

    def collect_changeset(self):
        # Dump existing changeset
        self.changeset = []

        cmd_get_commit_changeset = "git diff-tree --no-commit-id --name-status -r HEAD"
        cmd_get_commit_changeset_output =  self.command_helper.run_command(cmd_get_commit_changeset)

        changeset_list = [line for line in cmd_get_commit_changeset_output]

        if len(changeset_list) == 0:
            cmd_get_merged_changeset = "git diff-tree --no-commit-id --name-status -r HEAD^ HEAD^2"
            cmd_get_merged_changeset_output = self.command_helper.run_command(cmd_get_merged_changeset)

            changeset_list = [line for line in cmd_get_merged_changeset_output]

        if len(changeset_list) == 0:
            raise EmptyChangesetException

        # Process changeset
        for line in changeset_list:
            line_data = line.split("\t")
            file_action = line_data[0]
            file_path = line_data[1]

            if file_action == 'D':
                self.changeset.append({
                    'action': 'delete',
                    'file_path': file_path
                })
            elif file_action == 'M' or file_action == 'A':
                self.changeset.append({
                    'action': 'upload',
                    'file_path': file_path
                })
            else:
                self.changeset.append({
                    'action': 'upload',
                    'file_path': file_path
                })

        self.logger.log("---------------------- Collect Changeset Complete ----------------------")

        return self.changeset

    def process_changeset(self):
        result = self.sftp_helper.upload_files(self.changeset)

        self.logger.log("---------------------- Upload Changeset Complete ----------------------")
        self.logger.log("Number of files in changeset: {}".format(result["num_files"]))
        self.logger.log("Number of files uploaded successfully: {}".format(result["num_success"]))
        self.logger.log("Number of files deleted successfully: {}".format(result["num_deleted"]))
        self.logger.log("Number of files with errors: {}".format(result["num_errors"]))
        self.logger.log("Errors: {}".format(result["errors"]))

    def deploy(self):
        self.collect_changeset()

        self.process_changeset()

    def rollback(self):
        pass


class EmptyChangesetException(Exception):
    pass


class DeploymentException(Exception):
    pass