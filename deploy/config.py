import os

"""
App config.

Remote server connection info
Git settings
Logger settings
"""
class Config:
    def __init__(self):
        # Remote servers
        self.remote_servers = {
            'DEV': {
                'remote_server_host': '34.236.2.204', #"dev-ftp.discountmugs.com"
                'remote_server_user': 'rsync',
                'remote_server_use_pem_key': True,
                'remote_server_pem_key_path': os.path.realpath('./deploy/dm-dev-web.pem'),
                'remote_dir': '/dmsite/dev.discountmugs.com',  # Absolute path to directory where files will be uploaded
                'require_branch': 'DEV'
            },
            'QA': {
                'remote_server_host': '54.91.131.202', #"qa-ftp.discountmugs.com"
                'remote_server_user': 'deploy_qa',
                'remote_server_use_pem_key': True,
                'remote_server_pem_key_path': os.path.realpath('./deploy/dm-qa-web.pem'),
                'remote_dir': '/dmsite/dev',  # Absolute path to directory where files will be uploaded
                'require_branch': 'QA'
            },
            # TODO: this will be a dict of all prod servers, but for now we're testing with just one server
            'PROD': {
                'remote_server_host': "23.253.187.79", # discountmugs.com
                'remote_server_user': 'deploy_web08',
                'remote_server_use_pem_key': True,
                'remote_server_pem_key_path': os.path.realpath('./deploy/web8.pem'),
                'remote_dir': '/var/www/vhosts/discountmugs.com',  # Absolute path to directory where files will be uploaded
                'require_branch': 'PROD'
            }
        }

        # If a new folder is needs to ne created, the permission we create that folder with
        self.mkdir_permission = 755

        # This assumes our application is sitting in a folder in the root of the repo.
        # If this is not the case, ensure this is the absolute path to the root
        self.local_dir = "/".join(os.path.realpath(__file__).split("/")[:-2])

        # Logger config
        self.logger = {
            "core": {
                "print_while_logging": True,
                "print_at_end": True,
                "send_email": True
            },
            "email": {
                "smtp_server": "email-smtp.us-east-1.amazonaws.com",
                "smtp_port": 587,
                "smtp_user": "AKIAYAEJMYWWKLMJ6B4X",
                "smtp_pass": "BLIy10GFrLt3SqZ2jKfB/b+yF8KXZ2sjzQZUpwAvJE38",
                "recipients": "rnunez@belusallc.com"
            }
        }

        # App config
        self.app_name = 'BEL.Web.Deploy'
        self.current_environment = self.remote_servers["QA"]